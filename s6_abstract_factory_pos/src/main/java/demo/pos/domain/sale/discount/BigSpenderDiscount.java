package demo.pos.domain.sale.discount;

import demo.pos.domain.sale.Sale;

/**
 * @author Jan de Rijke.
 */
public class BigSpenderDiscount implements Discount {
	private double max;
	private double discount;

	public BigSpenderDiscount(double max,double discount) {
		this.max = max;
		this.discount = discount;
	}

	@Override
	public double getDiscount(double toPay) {
		return ( toPay >= max)?discount:0.0;
	}
}