package demo.pos.persistence;

import java.util.logging.Logger;

import static demo.pos.persistence.PersistenceType.MEMORY;

/**
 * @author Jan de Rijke.
 */
public class Repositories {
  private static Logger logger = Logger.getLogger("demo.pos.application.Pos");
  private  static RepositoryFactory factory;

  public static void init(PersistenceType persistence){
  	switch (persistence){
		  case MEMORY: factory =  new MemoryRepositoryFactory();	break;
		  case DB: factory = new DbRepositoryFactory();break;
	  }
  }

  public  static RepositoryFactory getFactory(){
  	if (factory == null){
  		init(MEMORY);
	  }
  	return factory;
  }




}
