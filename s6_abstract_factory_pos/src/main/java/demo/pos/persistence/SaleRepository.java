package demo.pos.persistence;

import demo.infra.Repository;
import demo.pos.domain.sale.Sale;

/**
 * Created by jan on 15/12/2016.
 */
public interface SaleRepository extends Repository<Long,Sale>{
}
