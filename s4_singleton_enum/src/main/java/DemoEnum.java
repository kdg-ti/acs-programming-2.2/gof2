import singleton.President;

public class DemoEnum {
    public static void main(String[] args) {
        //President test2 = new President("Barack Obama", "Democrates");

        President p = President.INSTANCE;
        p.setPresident("Donald Trump", "Republicans");
        System.out.println(p);

        President np = President.INSTANCE;
        np.setPresident("Joe Biden", "Democrats");
        System.out.println(p);
        System.out.println(np);
    }
}