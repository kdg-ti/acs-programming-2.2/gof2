package staticfactory;

public class PersonFactory {
    private PersonFactory() {
        // empty
    }

    static public Person newInstance(String name, String number) {
        return new Citizen(name, number);
    }

    static public Person newInstance(String name, NationalRegisterNumber number) {
        return new Citizen(name, number);
    }

    static public Person newInstance(String name) {
        return new Citizen(name, NationalRegisterNumber.INVALID_NUMBER);
    }
}
