package staticfactory;

public class BelgianNationalRegisterNumber implements NationalRegisterNumber {
    private String string;
    // private long nummer;

    public BelgianNationalRegisterNumber(String string) {
        this.string = string;
    }

    public long toLong() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            if (Character.isDigit(character)) {
                builder.append(character);
            }
        }
        return Long.parseLong(builder.toString());
    }

    public String toString() {
        return string;
    }
}

