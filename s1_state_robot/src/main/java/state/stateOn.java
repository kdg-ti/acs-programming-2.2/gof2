package state;

public class stateOn implements RobotState {
	private final Robot robot;
	
	public stateOn(Robot robot){
		this.robot = robot;
	}
	 
	@Override
	public void walk() {
		System.out.println("Walking...");
	}

	@Override
	public void cook() {
		System.out.println("Cooking...");
		robot.setState(new StateCook(robot));
	}

	@Override
	public void off() {
		robot.setState(new StateOff(robot));
		System.out.println("Robot is switched off");
	}

}
