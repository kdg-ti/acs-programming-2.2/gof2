package static_factory_method_refactor.constructor;

public class Student {
  int id;
  String name;
  double score;
  public Student(int id, String name, double score) {
    this.id = id;
    this.name = name.trim();
    this.score = score;
  }

}
